package eu.mytthew;

public class Book implements Lecture {
	private String title;

	public Book(String title) {
		this.title = title;
		addToLibrary(title);
	}

	@Override
	public void display() {
		System.out.println("Title " + title);
	}

	private void addToLibrary(String title) {
		System.out.println("Add " + title + " to library.");
	}
}
