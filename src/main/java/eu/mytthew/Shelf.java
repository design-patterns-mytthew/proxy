package eu.mytthew;

public class Shelf implements Lecture {
	private Book book;
	private String title;

	public Shelf(String title) {
		this.title = title;
	}

	@Override
	public void display() {
		if (book == null) {
			book = new Book(title);
		}
		book.display();
	}
}
